
;;使用git-emacs作为git插件
(add-to-list 'load-path (expand-file-name "elpa/plugins/git-emacs" user-emacs-directory))
(require 'git-emacs)

(provide 'init-git)
